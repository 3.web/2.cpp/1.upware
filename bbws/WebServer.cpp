#include <string>
#include <istream>
#include <sstream>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <vector>
#include <iterator>
#include <thread> 

#include "WebServer.h"
#include "config.h"
#include "firmware.h"
#include "./Model/HexFile.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "guid2str.h"

using namespace std;
using namespace rapidjson;

HexFileDb hexFileDb;

std::string userIdStr;

void wait() {
	std::this_thread::sleep_for(std::chrono::duration<int, std::ratio<1, 1000>>(200));
}

void WebServer::DatabaseInit(void){
	hexFileDb.Init();
}

void CreateNewFirmware(std::string firmwareId, std::string dataReceived) {
	
	Document d;
	d.Parse(dataReceived.c_str());
	std::string hexString = d["hexStr"].GetString();	
	std::string hexStringProcess = hexString;

	std::string delimiter = "\r\n";

	size_t pos = 0;
	std::string token;
	std::string blockStr = "";
	uint8_t indexOfBlockStr = 0;
	uint8_t blockIndex = 0;
	uint32_t firmwareStrLengthRemain = hexString.size();
	uint16_t totalFile = 0;

	hexStringProcess.erase(0, hexStringProcess.find(delimiter) + delimiter.length());
	uint32_t posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	while ((pos = hexStringProcess.find(delimiter)) != std::string::npos) {
		uint8_t sizeDataRemoving = pos + delimiter.length();
		if (pos > 43) {
			return;
		}
		token = hexStringProcess.substr(0, pos);
		hexStringProcess.erase(0, sizeDataRemoving);
		firmwareStrLengthRemain -= sizeDataRemoving;

		if ((token.size() == 15)&&(firmwareStrLengthRemain >= 34)) {
			token = "";
		}
		else{
			blockStr += token;
			indexOfBlockStr++;
			if (indexOfBlockStr == 50) {
				//std::cout << "New Block " << std::endl;
				//std::cout << blockStr << std::endl;

				HexFileObject hexFile;
				hexFile.firmwareId = firmwareId;
				hexFile.fileIndex = blockIndex;
				hexFile.hexStr = blockStr;
				hexFile.numberOfDownloads = 0;
				hexFileDb.Create(hexFile);

				blockStr = "";
				indexOfBlockStr = 0;
				blockIndex++;

				totalFile++;
			}
		}
	}
	// Bug
	//std::cout << "Final Block " << std::endl;
	//std::cout << blockStr << std::endl;
	//Bug
	blockStr += hexStringProcess;

	HexFileObject hexFile;
	hexFile.firmwareId = firmwareId;
	hexFile.fileIndex = blockIndex;
	hexFile.hexStr = blockStr;
	hexFile.numberOfDownloads = 0;
	hexFileDb.Create(hexFile);
	totalFile++;

	FirmwareStruct firmwareObj;
	firmwareObj.firmwareId = firmwareId;
	firmwareObj.name = d["name"].GetString();
	firmwareObj.version = d["version"].GetString();
	firmwareObj.platform = d["platform"].GetString();
	firmwareObj.userUid = d["userUid"].GetString();
	firmwareObj.hexStr = "";
	firmwareObj.fileCounts = totalFile;
	firmwareObj.downloads = 0;
	DbCreate(firmwareObj);
}
void UpdateFirmware(std::string dataReceived) {
	return;
	Document d;
	d.Parse(dataReceived.c_str());
	std::string hexString = d["hexStr"].GetString();
	std::string hexStringProcess = hexString;

	std::string firmwareId = d["firmwareId"].GetString();
	
	HexFileObject hexFileDel;
	hexFileDel.firmwareId = firmwareId;
	hexFileDb.Delete(hexFileDel);

	std::string delimiter = "\r\n";

	size_t pos = 0;
	std::string token;
	std::string blockStr = "";
	uint8_t indexOfBlockStr = 0;
	uint8_t blockIndex = 0;
	uint32_t firmwareStrLengthRemain = hexStringProcess.size();
	uint16_t totalFile = 0;

	hexStringProcess.erase(0, hexStringProcess.find(delimiter) + delimiter.length());
	uint32_t posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	posEndRemove = hexStringProcess.rfind(delimiter);
	hexStringProcess.erase(posEndRemove, firmwareStrLengthRemain - posEndRemove);
	while ((pos = hexStringProcess.find(delimiter)) != std::string::npos) {
		uint8_t sizeDataRemoving = pos + delimiter.length();
		if (pos > 43) {
			return;
		}
		token = hexStringProcess.substr(0, pos);
		hexStringProcess.erase(0, sizeDataRemoving);
		firmwareStrLengthRemain -= sizeDataRemoving;

		if ((token.size() == 15) && (firmwareStrLengthRemain >= 34)) {
			token = "";
		}
		else {
			blockStr += token;
			indexOfBlockStr++;
			if (indexOfBlockStr == 50) {
				//std::cout << "New Block " << std::endl;
				//std::cout << blockStr << std::endl;

				HexFileObject hexFile;
				hexFile.firmwareId = firmwareId;
				hexFile.fileIndex = blockIndex;
				hexFile.hexStr = blockStr;
				hexFile.numberOfDownloads = 0;
				hexFileDb.Create(hexFile);

				blockStr = "";
				indexOfBlockStr = 0;
				blockIndex++;

				totalFile++;
			}
		}
	}
	// Bug
	//std::cout << "Final Block " << std::endl;
	//std::cout << blockStr << std::endl;

	blockStr += hexStringProcess;

	HexFileObject hexFile;
	hexFile.firmwareId = firmwareId;
	hexFile.fileIndex = blockIndex;
	hexFile.hexStr = blockStr;
	hexFile.numberOfDownloads = 0;
	hexFileDb.Create(hexFile);
	totalFile++;

	FirmwareStruct firmwareObj;
	firmwareObj.firmwareId = d["firmwareId"].GetString();
	firmwareObj.name = d["name"].GetString();
	firmwareObj.version = d["version"].GetString();
	firmwareObj.platform = d["platform"].GetString();
	//firmwareObj.userUid = d["userUid"].GetString();
	firmwareObj.hexStr = "";
	firmwareObj.fileCounts = totalFile;
	//firmwareObj.downloads = 0;

	DbUpdate(firmwareObj);
}


// Handler for when a message is received from the client
void WebServer::onMessageReceived(int clientSocket, const char* msg, int length)
{
	// Parse out the client's request string e.g. GET /index.html HTTP/1.1
	std::istringstream iss(msg);
	std::vector<std::string> parsed((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;

	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;

	#ifdef DISPLAY_DATA_RECEIVED
		for (int i = 0; i < parsed.size(); i++) {
			cout << parsed[i];
		}
	#endif

	std::ostringstream oss;
	// If the GET request is valid, try and get the name
	//if (parsed.size() >= 3 && parsed[0] == "GET")
	#ifdef DEBUG
		cout << "\r\n------------------------------------------------";
		cout << "\r\nNew request, parsed size: "<<parsed.size()<<"\r\n";
	#endif
	if (parsed.size() >= 3)
	{
		htmlFile = parsed[1];

		// If the file is just a slash, use index.html. This should really
		// be if it _ends_ in a slash. I'll leave that for you :)

		if (htmlFile == "/"){
			std::ifstream t("introPage.htm");
			std::string str;

			t.seekg(0, std::ios::end);
			str.reserve(t.tellg());
			t.seekg(0, std::ios::beg);

			str.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << str.size() << "\r\n";
			oss << "\r\n";
			oss << str;
		}
		else if (htmlFile == "/login") {
			std::ifstream t("loginPage.htm");
			std::string str;

			t.seekg(0, std::ios::end);
			str.reserve(t.tellg());
			t.seekg(0, std::ios::beg);

			str.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << str.size() << "\r\n";
			oss << "\r\n";
			oss << str;

			//htmlFile = "/index.html";
			////content = "<!DOCTYPE html><html lang=\"en\"><head><title>upware login</title><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><link rel=\"icon\" href=\"https://upwareio.b-cdn.net/loginPage/images/icons/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/bootstrap/css/bootstrap.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/fonts/font-awesome-4.7.0/css/font-awesome.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/fonts/Linearicons-Free-v1.0.0/icon-font.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/animate/animate.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/css-hamburgers/hamburgers.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/animsition/css/animsition.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/select2/select2.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/vendor/daterangepicker/daterangepicker.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/css/util.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upwareio.b-cdn.net/loginPage/css/main.css\"></head><body><div class=\"limiter\"><div class=\"container-login100\" style=\"background-image: url('https://upwareio.b-cdn.net/loginPage/images/bckground.jpg');\"><div class=\"wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33\"><form class=\"login100-form validate-form flex-sb flex-w\"> <span class=\"login100-form-title p-b-53\"> Sign In With </span><a id='btn_google_login' href=\"#\" class=\"btn-google m-b-20\"> <img src=\"https://upwareio.b-cdn.net/loginPage/images/icons/icon-google.png\" alt=\"GOOGLE\"> Google </a><div class=\"p-t-31 p-b-9\"> <span class=\"txt1\"> Username </span></div><div class=\"wrap-input100 validate-input\" data-validate=\"Username is required\"> <input class=\"input100\" type=\"text\" name=\"username\"> <span class=\"focus-input100\"></span></div><div class=\"p-t-13 p-b-9\"> <span class=\"txt1\"> Password </span><a href=\"#\" class=\"txt2 bo1 m-l-5\"> Forgot? </a></div><div class=\"wrap-input100 validate-input\" data-validate=\"Password is required\"> <input class=\"input100\" type=\"password\" name=\"pass\"> <span class=\"focus-input100\"></span></div><div class=\"container-login100-form-btn m-t-17\"> <button class=\"login100-form-btn\"> Sign In </button></div><div class=\"w-full text-center p-t-55\"> <span class=\"txt2\"> Not a member? </span><a href=\"#\" class=\"txt2 bo1\"> Sign up now </a></div></form></div></div></div><div id=\"dropDownSelect1\"></div> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/animsition/js/animsition.min.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/bootstrap/js/popper.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/bootstrap/js/bootstrap.min.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/select2/select2.min.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/daterangepicker/moment.min.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/daterangepicker/daterangepicker.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/vendor/countdowntime/countdowntime.js\"></script> <script src=\"https://upwareio.b-cdn.net/loginPage/js/main.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-app.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-firestore.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-auth.js\"></script> </body> <script>function firebase_init(){var config={apiKey:\"AIzaSyC7elL3FYPlcVj7PvN9ecGpvVUVg6L1fKg\",authDomain:\"worklist-app.firebaseapp.com\",databaseURL:\"https://worklist-app.firebaseio.com\",projectId:\"worklist-app\",storageBucket:\"worklist-app.appspot.com\",messagingSenderId:\"740341135888\"};firebase.initializeApp(config);firebase.firestore().settings({});} var test1;var first_get=false;function google_login(){var provider=new firebase.auth.GoogleAuthProvider();firebase.auth().signInWithPopup(provider).then(function(result){var token=result.credential.accessToken;user=result.user;sendAjaxRegisterUser(user);window.location.href=\"/main\";}).catch(function(error){var errorCode=error.code;var errorMessage=error.message;var email=error.email;var credential=error.credential;});} function facebook_login(){var provider=new firebase.auth.FacebookAuthProvider();firebase.auth().signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(function(result){var token=result.credential.accessToken;user=result.user;sendAjaxRegisterUser(user);window.location.href=\"/main\";}).catch(function(error){if(error.code==='auth/account-exists-with-different-credential'){var pendingCred=error.credential;var email=error.email;firebase.auth().fetchSignInMethodsForEmail(email).then(function(methods){if(methods[0]==='password'){var password=promptUserForPassword();firebase.auth().signInWithEmailAndPassword(email,password).then(function(user){return user.linkWithCredential(pendingCred);}).then(function(){sendAjaxRegisterUser(user);window.location.href=\"/main\";});return;} var provider=new firebase.auth.GoogleAuthProvider();firebase.auth().signInWithPopup(provider).then(function(result){result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred){});});});}});} function sendAjaxRegisterUser(user){var obj={uid:user.uid,firmwareCount:0,};if(obj.uid!=undefined){$.ajax({url:\"/Home/userInit\",data:JSON.stringify(obj),type:\"POST\",contentType:\"application/json;charset=utf-8\",dataType:\"json\",success:function(result){},error:function(errormessage){}});}} $(document).ready(function(){firebase_init();firebase.auth().onAuthStateChanged(function(user){if(user){var user=firebase.auth().currentUser;sendAjaxRegisterUser(user);window.location.href=\"/main\";}else{}});document.getElementById('btn_google_login').addEventListener('click',google_login);})</script> <script></script> </html>";
			//content = "<!DOCTYPE html><html lang=\"en\"><head><title>upware login</title><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><link rel=\"icon\" href=\"https://upware1.b-cdn.net/loginPage/images/icons/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/bootstrap/css/bootstrap.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/fonts/font-awesome-4.7.0/css/font-awesome.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/fonts/Linearicons-Free-v1.0.0/icon-font.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/animate/animate.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/css-hamburgers/hamburgers.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/animsition/css/animsition.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/select2/select2.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/vendor/daterangepicker/daterangepicker.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/css/util.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"https://upware1.b-cdn.net/loginPage/css/main.css\"></head><body><div class=\"limiter\"><div class=\"container-login100\" style=\"background-image: url('https://upware1.b-cdn.net/loginPage/images/bckground.jpg');\"><div class=\"wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33\"><form class=\"login100-form validate-form flex-sb flex-w\"> <span class=\"login100-form-title p-b-53\"> Sign In With </span><a id='btn_google_login' href=\"#\" class=\"btn-google m-b-20\"> <img src=\"https://upware1.b-cdn.net/loginPage/images/icons/icon-google.png\" alt=\"GOOGLE\"> Google </a><div class=\"p-t-31 p-b-9\"> <span class=\"txt1\"> Username </span></div><div class=\"wrap-input100 validate-input\" data-validate=\"Username is required\"> <input class=\"input100\" type=\"text\" name=\"username\"> <span class=\"focus-input100\"></span></div><div class=\"p-t-13 p-b-9\"> <span class=\"txt1\"> Password </span><a href=\"#\" class=\"txt2 bo1 m-l-5\"> Forgot? </a></div><div class=\"wrap-input100 validate-input\" data-validate=\"Password is required\"> <input class=\"input100\" type=\"password\" name=\"pass\"> <span class=\"focus-input100\"></span></div><div class=\"container-login100-form-btn m-t-17\"> <button class=\"login100-form-btn\"> Sign In </button></div><div class=\"w-full text-center p-t-55\"> <span class=\"txt2\"> Not a member? </span><a href=\"#\" class=\"txt2 bo1\"> Sign up now </a></div></form></div></div></div><div id=\"dropDownSelect1\"></div> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/animsition/js/animsition.min.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/bootstrap/js/popper.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/bootstrap/js/bootstrap.min.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/select2/select2.min.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/daterangepicker/moment.min.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/daterangepicker/daterangepicker.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/vendor/countdowntime/countdowntime.js\"></script> <script src=\"https://upware1.b-cdn.net/loginPage/js/main.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-app.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-firestore.js\"></script> <script src=\"https://www.gstatic.com/firebasejs/7.2.3/firebase-auth.js\"></script> </body> <script>function firebase_init(){var config={apiKey:\"AIzaSyC7elL3FYPlcVj7PvN9ecGpvVUVg6L1fKg\",authDomain:\"worklist-app.firebaseapp.com\",databaseURL:\"https://worklist-app.firebaseio.com\",projectId:\"worklist-app\",storageBucket:\"worklist-app.appspot.com\",messagingSenderId:\"740341135888\"};firebase.initializeApp(config);firebase.firestore().settings({});} var test1;var first_get=false;function google_login(){var provider=new firebase.auth.GoogleAuthProvider();firebase.auth().signInWithPopup(provider).then(function(result){var token=result.credential.accessToken;user=result.user;sendAjaxRegisterUser(user);window.location.href=\"/main\";}).catch(function(error){var errorCode=error.code;var errorMessage=error.message;var email=error.email;var credential=error.credential;});} function facebook_login(){var provider=new firebase.auth.FacebookAuthProvider();firebase.auth().signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(function(result){var token=result.credential.accessToken;user=result.user;sendAjaxRegisterUser(user);window.location.href=\"/main\";}).catch(function(error){if(error.code==='auth/account-exists-with-different-credential'){var pendingCred=error.credential;var email=error.email;firebase.auth().fetchSignInMethodsForEmail(email).then(function(methods){if(methods[0]==='password'){var password=promptUserForPassword();firebase.auth().signInWithEmailAndPassword(email,password).then(function(user){return user.linkWithCredential(pendingCred);}).then(function(){sendAjaxRegisterUser(user);window.location.href=\"/main\";});return;} var provider=new firebase.auth.GoogleAuthProvider();firebase.auth().signInWithPopup(provider).then(function(result){result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred){});});});}});} function sendAjaxRegisterUser(user){var obj={uid:user.uid,firmwareCount:0,};if(obj.uid!=undefined){$.ajax({url:\"/Home/userInit\",data:JSON.stringify(obj),type:\"POST\",contentType:\"application/json;charset=utf-8\",dataType:\"json\",success:function(result){},error:function(errormessage){}});}} $(document).ready(function(){firebase_init();firebase.auth().onAuthStateChanged(function(user){if(user){var user=firebase.auth().currentUser;sendAjaxRegisterUser(user);window.location.href=\"/main\";}else{}});document.getElementById('btn_google_login').addEventListener('click',google_login);document.getElementById('btn_facebook_login').addEventListener('click',facebook_login);})</script> <script></script> </html>";
			//errorCode = 200;

			//oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			//oss << "Cache-Control: no-cache, private\r\n";
			//oss << "Content-Type: text/html\r\n";
			//oss << "Content-Length: " << content.size() << "\r\n";
			//oss << "\r\n";
			//oss << content;
		}
		else if (htmlFile == "/main") {
			std::ifstream t("main.htm");
			std::string str;

			t.seekg(0, std::ios::end);
			str.reserve(t.tellg());
			t.seekg(0, std::ios::beg);

			str.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << str.size() << "\r\n";
			oss << "\r\n";
			oss << str;
		}
		else if (htmlFile == "/main/Init") {
			//content = "[{\"id\":38,\"name\":\"Node 1\",\"firmwareId\":\"55b1f340-f328-427e-bf8a-038e16f55c8f\",\"version\":\"1.0.4\",\"dateTime\":\"\/Date(1599612554000)\/\",\"userUid\":\"a6hcH017Q4diBZCmn1KKQcAvMgB2\",\"platform\":\"STM32F103ZET6\",\"downloads\":12478,\"hexStr\":null,\"fileCounts\":0},{\"id\":42,\"name\":\"Firmware\",\"firmwareId\":\"24f6dbc8-598b-4c68-ab8c-de76738988cb\",\"version\":\"0.0.2\",\"dateTime\":\"\/Date(1599888487000)\/\",\"userUid\":\"a6hcH017Q4diBZCmn1KKQcAvMgB2\",\"platform\":\"STM32FXXX\",\"downloads\":2946,\"hexStr\":null,\"fileCounts\":0}]";
			content = DbGetOfUser((char*)userIdStr.c_str());
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;
		}
		else if (htmlFile == "/main/firmwareUpdate") {
			uint8_t index_t = parsed.size() - 1;
			std::string contentReceive = parsed[index_t];
			/*index_t++;
			while (index_t < parsed.size()) {
				contentReceive += " ";
				contentReceive += parsed[index_t];
				index_t++;
			}*/
			//Document d;
			//d.Parse(contentReceive.c_str());
			return;
			thread th1(UpdateFirmware, contentReceive);
			th1.detach();

			content = "\"OK:1\"";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;
		}
		else if (htmlFile == "/main/firmwareDelete") {
			//cout << "\r\nfirmwareDelete";
			//cout << parsed.size();
			uint8_t index_t = parsed.size() - 1;
			std::string contentReceive = parsed[index_t];
			/*index_t++;
			while (index_t < parsed.size()) {
				contentReceive += " ";
				contentReceive += parsed[index_t];
				index_t++;
			}*/
			//cout << "\r\n" << contentReceive;
			Document d;
			d.Parse(contentReceive.c_str());

			FirmwareStruct firmwareObj;
			firmwareObj.firmwareId = d["firmwareId"].GetString();

			DbDelete(firmwareObj);

			HexFileObject hexFile;
			hexFile.firmwareId = firmwareObj.firmwareId;
			hexFileDb.Delete(hexFile);
		}
		else if(htmlFile == "/main/firmwareUploaded"){
			content = "\"OK:1\"";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;
			Sleep(2000);
			//sendToClient(clientSocket, output.c_str(), size);
		}
		else if (htmlFile == "/main/firmwareCreate") {
			uint8_t index_t = parsed.size() - 1;
			std::string contentReceive = parsed[index_t];

			//std::cout << "\r\nData Received:\r\n";
			//std::cout << contentReceive;
			/*index_t++;
			while (index_t < parsed.size()) {
				contentReceive += " ";
				contentReceive += parsed[index_t];
				index_t++;
			}*/
			

			GUID gidReference;
			HRESULT hCreateGuid = CoCreateGuid(&gidReference);

			FirmwareStruct firmwareObj;
			string guidStr = GuidToString(gidReference);
			firmwareObj.firmwareId = guidStr.substr(1, guidStr.size() - 2);

			//CreateNewFirmware(firmwareObj.firmwareId, contentReceive);
			thread th1(CreateNewFirmware, firmwareObj.firmwareId, contentReceive); 
			th1.detach();

			content = '"' + firmwareObj.firmwareId + '"';
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;

			//sendToClient(clientSocket, output.c_str(), size);
		}
		else if (htmlFile == "/Home/userInit") {
			//userIdStr = parsed[parsed.size() - 1];
			uint8_t index_t = 0, startIndex_t, stopIndex_t;
			uint8_t element_t = parsed.size() - 1;
			uint8_t length_t = parsed[element_t].size();
			uint8_t stepExecute_t = 0;
			while (index_t < length_t) {
				if (parsed[element_t][index_t] == '"') {
					if (stepExecute_t == 2) {
						startIndex_t = index_t;
					}
					else if (stepExecute_t == 3) {
						stopIndex_t = index_t;
					}
					stepExecute_t++;
				}
				index_t++;
			}
			userIdStr = parsed[element_t].substr(startIndex_t + 1, stopIndex_t - startIndex_t - 1);
			//content = DbGetOfUser((char *)userIdStr.c_str());
			//content = "[{\"id\":38,\"name\":\"Node 1\",\"firmwareId\":\"55b1f340-f328-427e-bf8a-038e16f55c8f\",\"version\":\"1.0.4\",\"dateTime\":\"\/Date(1599612554000)\/\",\"userUid\":\"a6hcH017Q4diBZCmn1KKQcAvMgB2\",\"platform\":\"STM32F103ZET6\",\"downloads\":12478,\"hexStr\":null,\"fileCounts\":0},{\"id\":42,\"name\":\"Firmware\",\"firmwareId\":\"24f6dbc8-598b-4c68-ab8c-de76738988cb\",\"version\":\"0.0.2\",\"dateTime\":\"\/Date(1599888487000)\/\",\"userUid\":\"a6hcH017Q4diBZCmn1KKQcAvMgB2\",\"platform\":\"STM32FXXX\",\"downloads\":2946,\"hexStr\":null,\"fileCounts\":0}]";
			
			content = "{\"status\":\"ok\"}";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;
		}
		else if (htmlFile.substr(0, 9) == "/hex/data") {
			HexFileObject hexFile;
			hexFile.firmwareId = htmlFile.substr(10, 36);
			hexFile.fileIndex = std::stoi(htmlFile.substr(47, htmlFile.size() - 47));

			content = "\"" + hexFileDb.ReadData(hexFile) + "\"";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;

			sendToClient(clientSocket, output.c_str(), size);

			FirmwareStruct firmwareObj;
			firmwareObj.firmwareId = htmlFile.substr(10, 36);
			DbIncreaseDownloadCounts(firmwareObj);
		}
		else if (htmlFile.substr(0, 12) == "/hex/version") {
			FirmwareStruct firmwareObj;
			firmwareObj.firmwareId = htmlFile.substr(13, 36);

			content = "\"" + DbGetVersion(firmwareObj) + "\"";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;
		}
		else if (htmlFile.substr(0, 11) == "/hex/length") {
			FirmwareStruct firmwareObj;
			firmwareObj.firmwareId = htmlFile.substr(12, 36);

			content = "\"" + DbGetLength(firmwareObj) + "\"";
			errorCode = 200;

			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: private\r\n";
			oss << "Content-Type: application/json; charset=utf-8\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;
		}
		else {
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;
		}
	}
	else {

	}
	/*
	// Open the document in the local file system
	std::ifstream f(".\wwwroot" + htmlFile);

	// Check if it opened and if it did, grab the entire contents
	if (f.good())
	{
		std::string str((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
		content = str;
		errorCode = 200;
	}

	f.close();*/
    
	// Write the document back to the client
	
	

	std::string output = oss.str();
	int size = output.size() + 1;

	sendToClient(clientSocket, output.c_str(), size);
}

// Handler for client connections
void WebServer::onClientConnected(int clientSocket)
{

}

// Handler for client disconnections
void WebServer::onClientDisconnected(int clientSocket)
{
	
}