#pragma once
#include <ios>
#include <iostream>
#include "../config.h"
#include "../sqlite3.h"

typedef struct {
	uint16_t id;
	std::string firmwareId;
	uint16_t fileIndex;
	std::string hexStr;
	uint16_t numberOfDownloads;
}HexFileObject;

class HexFileDb {
public:
	HexFileDb();
	~HexFileDb();
	void Init(void);
	void Create(HexFileObject hexFileObj);
	std::string HexFileDb::ReadData(HexFileObject hexFileObj);
	std::string HexFileDb::ReadVersion(HexFileObject hexFileObj);
	void Update(HexFileObject hexFileObj);
	void Delete(HexFileObject hexFileObj);
private:
	sqlite3* db;
};