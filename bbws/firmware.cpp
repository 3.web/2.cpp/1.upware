#include "firmware.h"

using namespace std;

static sqlite3* db;
//char* dataGetOfUser;
void DbInit()
{
    int rc;
    char* error;

    // Open Database
    //cout << "Opening upware.db ..." << endl;
    rc = sqlite3_open("upware.db", &db);
    if (rc)
    {
        cerr << "Error opening SQLite3 database: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_close(db);
        return;
    }
    else
    {
        //cout << "Opened upware.db." << endl << endl;
    }


    // Execute SQL
    //cout << "Creating firmwareTb ..." << endl;
    const char* sqlCreateTable = "CREATE TABLE firmwareTb (id INTEGER PRIMARY KEY, userId STRING, firmwareId STRING, name STRING, platform STRING, version STRING, hexStr STRING, fileCount INTEGER, downloadCount INTEGER);";
    //const char* sqlCreateTable = "DROP TABLE firmwareTb;";
    rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
    if (rc)
    {
        cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }

    /*
        // Execute SQL
        cout << "Inserting a value into firmwareTb ..." << endl;
        //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
        const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
        rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
        if (rc)
        {
            cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
            sqlite3_free(error);
        }
        else
        {
            cout << "Inserted a value into firmwareTb." << endl << endl;
        }
    */
    
    /*
        // Execute SQL
        cout << "Inserting a value into firmwareTb ..." << endl;
        const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
        rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
        if (rc)
        {
            cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
            sqlite3_free(error);
        }
        else
        {
            cout << "Inserted a value into MyTable." << endl << endl;
        }
    */
    
    #ifdef DEBUG
        std::cout<<"\r\nOpen upware Database Successful";
    #endif
    // Display MyTable
    //cout << "Retrieving values in firmwareTb ..." << endl;
    const char* sqlSelect = "SELECT * FROM firmwareTb;";
    char** results = NULL;
    int rows, columns;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    // Display Table
    for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
    {
        for (int colCtr = 0; colCtr < columns; ++colCtr)
        {
            // Determine Cell Position
            int cellPosition = (rowCtr * columns) + colCtr;

            if (colCtr == 1) {
                // Display Cell Value
                cout.width(28);
                cout.setf(ios::left);
                cout << results[cellPosition] << " ";
            }
            else if (colCtr == 2) {
                // Display Cell Value
                cout.width(36);
                cout.setf(ios::left);
                cout << results[cellPosition] << " ";
            }
            else if (colCtr == 6) {
                // Not display hexString
            }
            else {
                // Display Cell Value
                cout.width(12);
                cout.setf(ios::left);
                cout << results[cellPosition] << " ";
            }
        }

        // End Line
        cout << endl;

        // Display Separator For Header
        if (0 == rowCtr)
        {
            for (int colCtr = 0; colCtr < columns; ++colCtr)
            {
                if (colCtr == 1) {
                    cout.width(28);
                    cout.setf(ios::left);
                    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                }
                else if (colCtr == 2) {
                    cout.width(36);
                    cout.setf(ios::left);
                    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                }
                else if (colCtr == 6) {
                    // Not display hexString
                }
                else {
                    cout.width(12);
                    cout.setf(ios::left);
                    cout << "~~~~~~~~~~~~ ";
                }
                
            }
            cout << endl;
        }
    }
    sqlite3_free_table(results);
    sqlite3_free(error);

    // Close Database
    //cout << "Closing upware.db ..." << endl;
    //sqlite3_close(db);
    //cout << "Closed upware.db" << endl << endl;
}
std::string DbGetOfUser(char* userId) {
    // Display MyTable
    //cout << "Retrieving values in firmwareTb ..." << endl;
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM firmwareTb WHERE userId='%s';", userId);
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    std::string dataReturn = "[";
    // Display Table
    for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
    {
        dataReturn += "{";
        dataReturn += "\"id\":";
        dataReturn += results[(rowCtr * columns) + 0];
        dataReturn += ",\"userUid\":\"";
        dataReturn += results[(rowCtr * columns) + 1];
        dataReturn += "\",\"firmwareId\":\"";
        dataReturn += results[(rowCtr * columns) + 2];
        dataReturn += "\",\"name\":\"";
        dataReturn += results[(rowCtr * columns) + 3];
        dataReturn += "\",\"platform\":\"";
        dataReturn += results[(rowCtr * columns) + 4];
        dataReturn += "\",\"version\":\"";
        dataReturn += results[(rowCtr * columns) + 5];
        dataReturn += "\",\"fileCounts\":";
        // +6 -> hexString -> not include
        dataReturn += results[(rowCtr * columns) + 7];
        dataReturn += ",\"downloads\":";
        dataReturn += results[(rowCtr * columns) + 8];
        dataReturn += ",\"hexStr\":null,\"dateTime\":\"\/Date(1599612554000)\/\"}";

        if (rowCtr < rows) {
            dataReturn += ",";
        }
    }
    dataReturn += "]";
    sqlite3_free_table(results);
    sqlite3_free(error);

    //dataGetOfUser = (char*)dataReturn.c_str();
    return dataReturn;
}
std::string DbGetVersion(FirmwareStruct firmwareStruct) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT version FROM firmwareTb WHERE firmwareId='%s';", firmwareStruct.firmwareId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    return results[1];
}
std::string DbGetLength(FirmwareStruct firmwareStruct) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT fileCount FROM firmwareTb WHERE firmwareId='%s';", firmwareStruct.firmwareId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    return results[1];
}
void DbCreate(FirmwareStruct firmwareStruct) {
    char* error;
    // Execute SQL
    //cout << "Inserting a value into firmwareTb ..." << endl;
    //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
    char sqlInsert[256];
    sprintf_s(sqlInsert, "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, hexStr, fileCount, downloadCount) VALUES('%s','%s','%s','%s','%s','%s',%d,%d);", 
        firmwareStruct.userUid.c_str(), 
        firmwareStruct.firmwareId.c_str(), 
        firmwareStruct.name.c_str(), 
        firmwareStruct.platform.c_str(), 
        firmwareStruct.version.c_str(), 
        firmwareStruct.hexStr.c_str(),
        firmwareStruct.fileCounts, 
        firmwareStruct.downloads);
    int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    if (rc)
    {
        cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Inserted a value into firmwareTb." << endl << endl;
    }
}
void DbUpdate(FirmwareStruct firmwareStruct) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE firmwareTb SET name='%s', platform='%s', version='%s', hexStr='%s', fileCount=%d WHERE firmwareId=\"%s\";", firmwareStruct.name.c_str(), firmwareStruct.platform.c_str(), firmwareStruct.version.c_str(), firmwareStruct.hexStr.c_str(), firmwareStruct.fileCounts, firmwareStruct.firmwareId.c_str());
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void DbIncreaseDownloadCounts(FirmwareStruct firmwareStruct) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE firmwareTb SET downloadCount=downloadCount + 1 WHERE firmwareId=\"%s\";", firmwareStruct.firmwareId.c_str());
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void DbDelete(FirmwareStruct firmwareStruct) {
    char* error;
    // Execute SQL
    //cout << "DbDelete ..." << endl;
    char sqlDelete[256];
    sprintf_s(sqlDelete, "DELETE FROM firmwareTb WHERE firmwareId='%s';", firmwareStruct.firmwareId.c_str());
    //cout << sqlDelete;
    int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
    if (rc)
    {
        cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}