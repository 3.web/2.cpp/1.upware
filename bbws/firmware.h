#pragma once
#include <ios>
#include <iostream>
#include "config.h"
#include "sqlite3.h"

typedef struct {
	uint16_t id;
	std::string userUid;
	std::string firmwareId;
	std::string name;
	std::string platform;
	std::string version;
	std::string hexStr;
	uint8_t fileCounts;
	uint16_t downloads;
}FirmwareStruct;

void DbInit();
std::string DbGetOfUser(char* userId);
std::string DbGetVersion(FirmwareStruct firmwareStruct);
std::string DbGetLength(FirmwareStruct firmwareStruct);
void DbCreate(FirmwareStruct firmwareStruct);
void DbUpdate(FirmwareStruct firmwareStruct);
void DbIncreaseDownloadCounts(FirmwareStruct firmwareStruct);
void DbDelete(FirmwareStruct firmwareStruct);